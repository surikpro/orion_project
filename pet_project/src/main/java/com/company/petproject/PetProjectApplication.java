package com.company.petproject;


import com.company.petproject.controller.UserController;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class PetProjectApplication {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("XmlApplicationContext.xml");
//        UserController userController = null;
//        userController.create(input);
//        System.out.println(userController.get("13cce263-6e89-4fe6-beac-ca80ebe6d63d"));
        String input = "{\"firstName\":\"Aydar\"}";
        UserController userController = context.getBean("userController", UserController.class);
        System.out.println(userController.get("13cce263-6e89-4fe6-beac-ca80ebe6d63d"));


    }

}
