package com.company.petproject.service.Impl;

import com.company.petproject.domain.entity.User;
import com.company.petproject.repository.UserRepository;
import com.company.petproject.service.UserService;
import lombok.RequiredArgsConstructor;

import java.util.UUID;
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Override
    public User get(UUID id) {
        return userRepository.get(id);
    }

    @Override
    public User create(User user) {
        return userRepository.create(user);
    }

    @Override
    public User update(UUID id, User user) {
        return userRepository.update(user.withId(id));
    }

    @Override
    public void delete(UUID id) {
        userRepository.delete(id);
    }
}
