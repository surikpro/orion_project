package com.company.petproject.controller;


import com.company.petproject.domain.entity.User;
import com.company.petproject.service.Impl.UserServiceImpl;
import com.company.petproject.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import java.util.UUID;

@RequiredArgsConstructor
public class UserController {
    private final ObjectMapper objectMapper;
    private final UserService userService;

    @SneakyThrows
    public String get(String id) {
        return objectMapper.writeValueAsString(userService.get(UUID.fromString(id)));
    }
    @SneakyThrows
    public String create(String userJson) {
        User user = objectMapper.readValue(userJson, User.class);
        User created = userService.create(user);
        return objectMapper.writeValueAsString(created);
    }
    @SneakyThrows
    public String update(String id, String userJson) {
        User user = objectMapper.readValue(userJson, User.class);
        User created = userService.update(UUID.fromString(id), user);
        return objectMapper.writeValueAsString(created);
    }
    public void delete(String id) {
        userService.delete(UUID.fromString(id));
    }
}